#include <Arduino.h>

// define default parameter
int defaultWaitToStart = 10000; // ******* ACHTUNG ****** im Wettbewerb muss 10 Sekunden gewartet werden nach Anschalten bis zum ersten Losfahren!
int defaultSpeed = 100;

// motor right (Gleichstrommotor 1)
#define GSM1 10 //verwenden für pins
int in1 = 9;//  in1 --> motorRightIn1
int in2 = 8;
float motorRightFactor = 1.15; // Karrosserie schleift rechts vorne am  Boden, deshalb muss der rechte Motor um motorRightFactor mehr drehen als der linke Motor

// motor left (Gleichstrommotor 2)
int GSM2 = 5;
int in3 = 7;
int in4 = 6;

// front IR-Sensor digital out
#define ir_front 3// schwarz

// back IR-Sensor digital out
#define ir_back 2 // orange

int white_front = 1;  //1 = black
int white_back = 1; //1 = black
/*
#define ir_back 52
#define ir_front 53    sensor test
int whitew = 0;
*/
/*********************************************************
   Driving Functions
 *********************************************************/

void forward (int Speed) {
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);

  analogWrite(GSM1, Speed * motorRightFactor);

  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);

  analogWrite(GSM2, Speed);
}

void backward (int Speed) {
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);

  analogWrite(GSM1, Speed * motorRightFactor);

  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);

  analogWrite(GSM2, Speed);
}

void stopMotor() {
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);

  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
}

void rotateRight (int Speed) {
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);

  analogWrite(GSM1, Speed * motorRightFactor);

  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);

  analogWrite(GSM2, Speed);
}

void rotateLeft (int Speed) {
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);

  analogWrite(GSM1, Speed * motorRightFactor);

  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);

  analogWrite(GSM2, Speed);
}

void frontInterrupt() {
  while (!digitalRead(ir_front)) {
    backward(defaultSpeed);
  }
  delay(500);
}

void backInterrupt() {
  while (!digitalRead(ir_back)) {
    forward(defaultSpeed * 2);
  }
  delay(500);
}

void setup() {
  Serial.begin(115200);// Setzt serielle Verbindung auf
  Serial.println("Init begin");//init = initialisieren = alles auf default setzen (Schreibt in die Konsole[oben rechts])

  pinMode(GSM1, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);

  pinMode(GSM2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  pinMode(ir_front, INPUT);
  pinMode(ir_back, INPUT);

  attachInterrupt(digitalPinToInterrupt(ir_front), frontInterrupt, FALLING);
  attachInterrupt(digitalPinToInterrupt(ir_back), backInterrupt, FALLING);

  stopMotor();
  
  delay(defaultWaitToStart);  // wait some seconds before start
  forward(defaultSpeed); // normal mode: always drive forward

  Serial.println("Init done");
  
}

void loop() {
  //Serial.println("loop begin");
  forward(defaultSpeed);
  delay(3000);
  rotateRight(defaultSpeed);
  delay(1000);
  forward(defaultSpeed);
  delay(3000);
  rotateLeft(defaultSpeed);
  delay(1000);
  
/*   if (!digitalRead(ir_front)) {
    Serial.println("White line at front detected");
    stopMotor(); // stop! line in front of robot has been detected!

    backward(defaultSpeed);
    for( int i = 0; i< 1000; i++) {
     delay(1);
     if (!digitalRead(ir_front)) {
      backward(defaultSpeed*1.5);
      i=0;
     }
     if (!digitalRead(ir_back)) {
      break;
     }
     backward(defaultSpeed);
     } 
    rotateRight(defaultSpeed); // turn clockwise
    for( int i = 0; i< 1000; i++) {
     delay(1);
     if (!digitalRead(ir_front)) {
      rotateRight(defaultSpeed*1.5);
      i=0;
     }
     rotateRight(defaultSpeed);
     } 
    
    forward(defaultSpeed); // return to normal forward driving
  }

  if (!digitalRead(ir_back)) {
    Serial.println("White line at back detected");
    stopMotor(); // stop! line behind robot has been detected
    forward(100); // are we pushed by enemy robot? drive forward slowly to insure grip! !!!Not slow any more!!!
    delay(1000);

    forward(defaultSpeed); // return to normal forward driving
  } */
  
}